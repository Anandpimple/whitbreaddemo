Owner : Anand D. Pimple
Email : anand.pimple@gmail.com

Notes :
    1. Please do provide me feedback as it will help me in improving upon my skill. Thank you :)
    2. Was expected to take maximum of 4 hrs but i exceeded it by 1 hrs or so.
    3. There is big gap in check-ins as i started working on test and was required to stop work because
       of urgent personal issue. Resumed work on monday evening and checked in in one go. 


Goal : To develop a microservice for consuming Foresquare api and return a list of point of interests (poi) near location
specified.

Assumption : Its important to show capability to fetch and show data from api. Except name and Id, i omited all other data
for simplicity.

Not Implemented : 
    1. Service should guard against xss attack
    2. Should use foursquare java api to call endpoints. Api has issues, hence not used.
    3. As this is a microservice, logger should have appender writing to logstash (i.e. Use ELK stack for centralized logging)
    4. Should have included dockerfile to create image for running this micoroservice on container
    3. Secure communication over https

Software required to run this service:
   1. JDK 1.8, Maven, availability of port 8080 on m/c     

How to run :
    1. Clone project at required location
    2. cd to project directory
    3. mvn spring-boot:run
    4. Http get request to http://localhost:8080/locations/search/poi?location=<any_location>
    
Description:

   1. Started with TDD approach. I.e. Developing obvious test cases and implementation for the controller.
   2. Refactored code using S.O.L.I.D principal
   