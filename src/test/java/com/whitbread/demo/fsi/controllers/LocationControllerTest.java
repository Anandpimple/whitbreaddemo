package com.whitbread.demo.fsi.controllers;

import com.whitbread.demo.fsi.configs.AppConfig;
import com.whitbread.demo.fsi.dtos.Venue;
import com.whitbread.demo.fsi.services.LocationService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class LocationControllerTest  {
    @Autowired
    private WebApplicationContext webApplicationContext;

    private LocationService locationService;

    private RequestIdGenerator<String> generator = () -> "dummy_id";

    private MockMvc mockMvc;

    @Before
    public void setup() {
        locationService = mock(LocationService.class);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        webApplicationContext.getBean(LocationController.class).setLocationService(locationService).setRequestIdGenerator(generator);
    }
    @After
    public void tearDown(){
        mockMvc = null;
        locationService = null;
    }
    @Test
    public void poiNearLocation_noLocationParameterPassed() throws Exception {
        String expected = "{\"response\":null,\"information\":{\"status\":400,\"errorCode\":1,\"errorMsg\":\"Some of the required parameters are missing\",\"errorDetails\":\"Required String parameter 'location' is not present\",\"requestId\":null}}";
       // when(locationService.searchPoiNear(any())).thenReturn(new Venue().setName("Test"));
        String response = mockMvc.perform(get("/locations/search/poi")).andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();
        //Assert.assertEquals(expected, response);
    }
    @Test
    public void poiNearLocation_noLocationParameterPassedWithEmptyValue() throws Exception {
        String expected = "{\"response\":null,\"information\":{\"status\":400,\"errorCode\":1,\"errorMsg\":\"Some of the required parameters are missing\",\"errorDetails\":\"Required String parameter 'location' is not present\",\"requestId\":null}}";
        //when(locationService.searchPoiNear(any())).thenReturn(new Venue().setName("Test"));
        String response = mockMvc.perform(get("/locations/search/poi?location")).andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();
        Assert.assertEquals(expected,response);
        //Mockito.verify(locationService).;
    }
    @Test
    public void poiNearLocation_venueFound() throws Exception {
        String expected = "{\"response\":{\"name\":\"Test\",\"id\":null,\"nearbyPois\":[]},\"information\":{\"status\":200,\"errorCode\":0,\"errorMsg\":null,\"errorDetails\":null,\"requestId\":\"dummy_id\"}}";
        when(locationService.searchPoiNear(any())).thenReturn(new Venue().setName("Test"));
        String response = mockMvc.perform(get("/locations/search/poi?location=1")).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        Assert.assertEquals(expected,response);
    }

    @Test
    public void poiNearLocation_OtherRuntimeException() throws Exception {
        String expected = "{\"response\":null,\"information\":{\"status\":500,\"errorCode\":1,\"errorMsg\":\"Technical issue occurred. Try after some time\",\"errorDetails\":\"Technical issue: Try Later\",\"requestId\":\"dummy_id\"}}";
        when(locationService.searchPoiNear(any())).thenThrow(new IllegalArgumentException("Some runtime exception"));
        String response = mockMvc.perform(get("/locations/search/poi?location=1")).andExpect(status().isInternalServerError()).andReturn().getResponse().getContentAsString();
        Assert.assertEquals(expected,response);
    }
}
