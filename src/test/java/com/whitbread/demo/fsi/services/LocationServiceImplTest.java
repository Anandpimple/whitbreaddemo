package com.whitbread.demo.fsi.services;

import com.whitbread.demo.fsi.builders.UrlBuilder;
import com.whitbread.demo.fsi.convertors.DataConverter;
import com.whitbread.demo.fsi.dtos.Venue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class LocationServiceImplTest {
    private LocationServiceImpl locationService;
    private DataConverter converter;
    private UrlBuilder urlBuilder;
    private RestTemplate restTemplate;

    @Before
    public void setUp(){

        locationService = new LocationServiceImpl();
        locationService.setUrlBuilder(urlBuilder = mock(UrlBuilder.class));
        locationService.setConverter(converter = mock(DataConverter.class));
        locationService.setRestTemplate(restTemplate = mock(RestTemplate.class));
    }
    @After
    public void tearDown(){
        urlBuilder = null;
        converter = null;
        restTemplate = null;
        locationService = null;
    }
    @Test(expected = IllegalArgumentException.class)
    public void testSearchPoiNearArgumentNull(){
        locationService.searchPoiNear(null);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testSearchPoiNearArgumentNullLocation(){
        locationService.searchPoiNear(new Venue());
    }
    @Test
    public void testSearchPoiNearSuccess(){
        Venue expected = new Venue().addPoi(new Venue().setName("poi")).setName("test_location");
        when(urlBuilder.buildUrl(any(),any())).thenReturn("TestApiUrl");
        when(restTemplate.getForObject(any(),any())).thenReturn("Json Data");
        when(converter.convert(any())).thenReturn(new Venue().addPoi(new Venue().setName("poi")));
        Venue actual = locationService.searchPoiNear(new Venue().setName("test_location"));
        assertEquals(expected.getName(),actual.getName());
        assertEquals(expected.getNearbyPois().get(0).getName(),actual.getNearbyPois().get(0).getName());
    }
}