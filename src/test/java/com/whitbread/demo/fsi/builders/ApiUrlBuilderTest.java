package com.whitbread.demo.fsi.builders;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ApiUrlBuilderTest {
    private static final String QUESTION = "\\?";
    private Environment environment;
    @Before
    public void setUp(){
        environment = mock(Environment.class);
    }
    @After
    public void tearDown(){
        environment = null;
    }
    @Test(expected = IllegalArgumentException.class)
    public void testBuildUrlWithNullApi() throws Exception {
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder();
        apiUrlBuilder.setEnvironment(environment);
        apiUrlBuilder.buildUrl(null,new HashMap<>());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBuildUrlWithEmptyApi() throws Exception {
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder();
        apiUrlBuilder.setEnvironment(environment);
        apiUrlBuilder.buildUrl(" ",new HashMap<>());
    }

    @Test
    public void testBuildUrlWithApiAndNullParams() throws Exception {
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder();
        apiUrlBuilder.setEnvironment(environment);
        when(environment.getProperty(anyString())).thenReturn("dummy_url");
        assertEquals("dummy_url",apiUrlBuilder.buildUrl("dummy",null));
    }
    @Test
    public void testBuildUrlWithApiAndEmptyParams() throws Exception {
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder();
        apiUrlBuilder.setEnvironment(environment);
        when(environment.getProperty(anyString())).thenReturn("dummy_url");
        assertEquals("dummy_url",apiUrlBuilder.buildUrl("dummy",new HashMap<>()));
    }
    @Test
    public void testBuildUrlWithApiAndParamsSingleValue() throws Exception {
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder();
        apiUrlBuilder.setEnvironment(environment);
        when(environment.getProperty(anyString())).thenReturn("dummy_url");
        Map<String,String> params = new HashMap<>();
        params.put("first_key","first_value");
        assertEquals("dummy_url?first_key=first_value",apiUrlBuilder.buildUrl("dummy",params));
    }
    @Test
    public void testBuildUrlWithApiAndParamsMultipeValue() throws Exception {
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder();
        apiUrlBuilder.setEnvironment(environment);
        when(environment.getProperty(anyString())).thenReturn("dummy_url");
        Map<String,String> params = new HashMap<>();
        params.put("first_key","first_value");
        params.put("second_key","second_value");
        params.put("third_key","third_value");
        params.put("fourth_key","fourth_value");
        String [] expected = new String[]{"first_key=first_value","second_key=second_value",
                "third_key=third_value","fourth_key=fourth_value"};
        testMultipleParams(apiUrlBuilder.buildUrl("dummy",params),"dummy_url", expected);
    }

    @Test
    public void testBuildUrlWithApiAndParamsMultipeValueWithEmptyAndNullVal() throws Exception {
        ApiUrlBuilder apiUrlBuilder = new ApiUrlBuilder();
        apiUrlBuilder.setEnvironment(environment);
        when(environment.getProperty(anyString())).thenReturn("dummy_url");
        Map<String,String> params = new HashMap<>();
        params.put("not_included1"," ");
        params.put("first_key","first_value");
        params.put("not_included2",null);
        params.put("second_key","second_value");
        params.put("third_key","third_value");
        params.put(null,null);
        params.put("fourth_key","fourth_value");
        String [] expected = new String[]{"first_key=first_value","second_key=second_value",
                "third_key=third_value","fourth_key=fourth_value"};
        testMultipleParams(apiUrlBuilder.buildUrl("dummy",params),"dummy_url", expected);
    }

    private void testMultipleParams(String url, String baseUrl, String [] values){
        String [] data1 = url.split(QUESTION);
        assertEquals(baseUrl,data1[0]);
        List<String> data2 = Arrays.asList(data1[1].split("&"));
        assertEquals(values.length,data2.size());
        for (String val : values){
            assertEquals(true,data2.contains(val));
        }

    }

}