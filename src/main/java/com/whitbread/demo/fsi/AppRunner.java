package com.whitbread.demo.fsi;

import com.whitbread.demo.fsi.configs.AppConfig;
import com.whitbread.demo.fsi.configs.CustomWebSecurityContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppRunner {
    public static void main(String[] args) {
        SpringApplication.run(new Object[]{AppConfig.class,CustomWebSecurityContext.class},args);
    }
}
