package com.whitbread.demo.fsi.services;

import com.whitbread.demo.fsi.dtos.Venue;

public interface LocationService {
    Venue searchPoiNear(Venue location);
}
