package com.whitbread.demo.fsi.services;

import com.whitbread.demo.fsi.builders.UrlBuilder;
import com.whitbread.demo.fsi.convertors.DataConverter;
import com.whitbread.demo.fsi.dtos.Venue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class LocationServiceImpl implements LocationService {
    private final static Logger logger = LoggerFactory.getLogger(LocationServiceImpl.class);
    private static final String FSI = "fsi";
    private static final String NEAR = "near";
    @Autowired
    private UrlBuilder urlBuilder;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    @Qualifier("venueDataConverter")
    private DataConverter<Map<String, Object>, Venue> converter;

    @Override
    public Venue searchPoiNear(Venue location) {
        if (null == location || !StringUtils.hasText(location.getName()))
            throw new IllegalArgumentException("Location is required");
        if (logger.isDebugEnabled())
            logger.debug("Searching for venue : " + location.getName());
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put(NEAR, location.getName());
        String url = urlBuilder.buildUrl(FSI, queryParams);
        if (logger.isDebugEnabled())
            logger.info("Url: " + url);
        return converter.convert(restTemplate.getForObject(url, Map.class)).setName(location.getName());

    }


    LocationService setUrlBuilder(UrlBuilder apiUrlBuilder) {
        this.urlBuilder = apiUrlBuilder;
        return this;
    }


    LocationService setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        return this;
    }


    LocationService setConverter(DataConverter<Map<String, Object>, Venue> converter) {
        this.converter = converter;
        return this;
    }
}
