package com.whitbread.demo.fsi.controllers;

import com.whitbread.demo.fsi.dtos.Response;
import com.whitbread.demo.fsi.dtos.ResponseDetails;
import com.whitbread.demo.fsi.dtos.Venue;
import com.whitbread.demo.fsi.exceptions.InternalServerException;
import com.whitbread.demo.fsi.exceptions.RequestDataException;
import com.whitbread.demo.fsi.exceptions.VenueNotFoundException;
import com.whitbread.demo.fsi.services.LocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;


@RestController
@RequestMapping("locations")
public class LocationController {
    private final static Logger logger = LoggerFactory.getLogger(LocationController.class);
    @Autowired
    private LocationService locationService;
    @Autowired
    private RequestIdGenerator<String> requestIdGenerator;
    @Cacheable("locationPoiSearch")
    @RequestMapping(value = "/search/poi", method = RequestMethod.GET)
    @ResponseStatus(code = HttpStatus.OK)
    public @ResponseBody ResponseEntity<Response<Venue>> searchPoi(@RequestParam String location){
        String requestId = requestIdGenerator.generateId();
        if(!StringUtils.hasText(location))
            throw new RequestDataException("Location is mandatory",requestId);
        try {
            Venue venue = locationService.searchPoiNear(new Venue().setName(location));

        return new ResponseEntity<Response<Venue>>(new Response().
                setInformation(new ResponseDetails().setStatus(HttpStatus.OK.value()).setRequestId(requestId)).setResponse(venue)
        ,HttpStatus.OK);
        }catch(Throwable e){
            logger.error("Error while processing Request Id : "+requestId,e);
            throw e instanceof HttpClientErrorException ?
                    new VenueNotFoundException("Location "+location+" not found").setUuid(requestId):
                    new InternalServerException("Technical issue: Try Later").setUuid(requestId);
        }
    }

    LocationController setLocationService(LocationService locationService) {
        this.locationService = locationService;
        return this;
    }

    LocationController setRequestIdGenerator(RequestIdGenerator<String> requestIdGenerator) {
        this.requestIdGenerator = requestIdGenerator;
        return this;
    }
}
