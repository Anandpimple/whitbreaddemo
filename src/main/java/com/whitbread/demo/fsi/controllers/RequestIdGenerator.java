package com.whitbread.demo.fsi.controllers;

public interface RequestIdGenerator<O> {
    O generateId();
}
