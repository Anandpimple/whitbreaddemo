package com.whitbread.demo.fsi.controllers;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UUIDRequestIdGenerator implements RequestIdGenerator<String> {
    @Override
    public String generateId() {
        return UUID.randomUUID().toString();
    }
}
