package com.whitbread.demo.fsi.advices;

import com.whitbread.demo.fsi.dtos.Response;
import com.whitbread.demo.fsi.dtos.ResponseDetails;
import com.whitbread.demo.fsi.exceptions.InternalServerException;
import com.whitbread.demo.fsi.exceptions.RequestDataException;
import com.whitbread.demo.fsi.exceptions.VenueNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class RestExceptionHandlerAdvice{

    private static final String BODY_OF_RESPONSE = "Some of the required parameters are missing";
    private static final String DATA_NOT_FOUND = "Data not found";

    @ExceptionHandler(value = { RequestDataException.class})
    protected ResponseEntity<Response> requiredParametersMissing(RequestDataException ex, WebRequest request) {
        return new ResponseEntity<>(new Response().setInformation(new ResponseDetails().
                setStatus(HttpStatus.BAD_REQUEST.value()).setErrorCode(1).
                setErrorMsg(BODY_OF_RESPONSE).setErrorDetails(ex.getMessage()).setRequestId(ex.getUuid())),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = { MissingServletRequestParameterException.class})
    protected ResponseEntity<Response> requiredParametersMissing(Exception ex, WebRequest request) {
        return new ResponseEntity<>(new Response().setInformation(new ResponseDetails().
                setStatus(HttpStatus.BAD_REQUEST.value()).setErrorCode(1).
                setErrorMsg(BODY_OF_RESPONSE).setErrorDetails(ex.getMessage())),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = { VenueNotFoundException.class})
    protected ResponseEntity<Response> venueNotFound(VenueNotFoundException ex, WebRequest request) {
        return new ResponseEntity<>(new Response().setInformation(new ResponseDetails().
                setStatus(HttpStatus.NOT_FOUND.value()).setErrorCode(1).
                setErrorMsg(DATA_NOT_FOUND).setErrorDetails(ex.getMessage()).setRequestId(ex.getUuid())),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = { InternalServerException.class})
    protected ResponseEntity<Response> internalServerException(InternalServerException ex, WebRequest request) {
        return new ResponseEntity<>(new Response().setInformation(new ResponseDetails().
                setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value()).setErrorCode(1).
                setErrorMsg("Technical issue occurred. Try after some time").
                setErrorDetails(ex.getMessage()).setRequestId(ex.getUuid())),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
