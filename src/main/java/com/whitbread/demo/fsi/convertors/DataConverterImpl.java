package com.whitbread.demo.fsi.convertors;

import com.whitbread.demo.fsi.dtos.Venue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

@Component("venueDataConverter")
public class DataConverterImpl implements DataConverter<Map<String,Object>,Venue> {
    private final static Logger logger = LoggerFactory.getLogger(DataConverterImpl.class);
    @Override
    public Venue convert(Map<String, Object> stringObjectMap) {
        if(logger.isDebugEnabled())
            logger.debug("Venue search response :> "+stringObjectMap);
        if(null != stringObjectMap && !stringObjectMap.isEmpty()){
            Venue venue = new Venue();
            Optional.ofNullable(stringObjectMap.get("response")).ifPresent(data -> populateVenues((Map<String,ArrayList<Map>>)data,venue));
            return venue;
        }else
            return null;
    }
    private void populateVenues(Map<String,ArrayList<Map>> data,Venue venue){
        Optional.ofNullable(data.get("venues")).ifPresent(
                list -> list.parallelStream().filter(value -> null != value && !value.isEmpty()).
                        forEach(value1 -> addVenue(value1,venue)));
    }
    private void addVenue(Map<String,Object> data, Venue venue){
        if(logger.isDebugEnabled())
            logger.debug("Venue data :> "+data);
        venue.addPoi(new Venue().setId(data.get("id").toString()).setName(data.get("name").toString()));
    }
}
