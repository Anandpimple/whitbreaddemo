package com.whitbread.demo.fsi.convertors;

import java.io.Serializable;

public interface DataConverter<I,O extends Serializable>{
    O convert(I i);
}
