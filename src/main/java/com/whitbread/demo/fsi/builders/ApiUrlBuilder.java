package com.whitbread.demo.fsi.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.Optional;

@Component
public class ApiUrlBuilder implements UrlBuilder{
    private static final String QUESTION = "?";
    private static final String AND = "&";
    private static final String EQUAL = "=";
    private static final String BASE_URL = "BaseUrl";
    @Autowired
    private Environment environment;
    @Override
    public String buildUrl(String apiName, Map<String, String> params) {
        if(!StringUtils.hasText(apiName))
            throw new IllegalArgumentException("Api name is required");
        else{
            StringBuilder builder = new StringBuilder(environment.getProperty(apiName+ BASE_URL));
            Optional.ofNullable(params).ifPresent(value -> appendParams(builder,value));
            return builder.toString();
        }
    }
    private void appendParams(StringBuilder builder,Map<String, String> params){
        boolean isFirst = true;
        for(Map.Entry<String,String> entry : params.entrySet()){
            if(StringUtils.hasText(entry.getKey()) && StringUtils.hasText(entry.getValue())){
                builder.append(isFirst?QUESTION:AND).append(entry.getKey()).append(EQUAL).append(entry.getValue());
                isFirst = false;
            }
        }
    }

    ApiUrlBuilder setEnvironment(Environment environment) {
        this.environment = environment;
        return this;
    }
}
