package com.whitbread.demo.fsi.builders;

import java.util.Map;

public interface UrlBuilder {
    String buildUrl(String apiName, Map<String,String> params);
}
