package com.whitbread.demo.fsi.exceptions;

public class RestException extends RuntimeException {
    private String uuid;
    public RestException(String message){
        super(message);
    }

    public String getUuid() {
        return uuid;
    }

    public RestException setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }
}
