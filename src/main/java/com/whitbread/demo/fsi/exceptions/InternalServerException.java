package com.whitbread.demo.fsi.exceptions;

public class InternalServerException extends RestException{
    public InternalServerException(String msg){
        super(msg);
    }
}
