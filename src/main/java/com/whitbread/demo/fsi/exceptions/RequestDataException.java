package com.whitbread.demo.fsi.exceptions;

public class RequestDataException extends RestException{
    public RequestDataException(String message, String uuid) {
        super(message);
    }
}
