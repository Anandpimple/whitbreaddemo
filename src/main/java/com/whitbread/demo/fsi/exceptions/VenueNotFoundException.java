package com.whitbread.demo.fsi.exceptions;

public class VenueNotFoundException extends RestException {
    public VenueNotFoundException(String message){
        super(message);
    }
}
