package com.whitbread.demo.fsi.dtos;

import java.io.Serializable;

public class Response<O> implements Serializable {
    private O response;
    private ResponseDetails information;

    public O getResponse() {
        return response;
    }

    public Response<O> setResponse(O response) {
        this.response = response;
        return this;
    }

    public ResponseDetails getInformation() {
        return information;
    }

    public Response<O> setInformation(ResponseDetails information) {
        this.information = information;
        return this;
    }
}
