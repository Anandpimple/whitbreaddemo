package com.whitbread.demo.fsi.dtos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Venue implements Serializable {
    private String name;
    private String id;
    //Point of interests near this location.
    private List<Venue> nearbyPois = new ArrayList<>();

    public String getName() {
        return name;
    }

    public Venue setName(String name) {
        this.name = name;
        return this;
    }

    public String getId() {
        return id;
    }

    public Venue setId(String id) {
        this.id = id;
        return this;
    }

    public List<Venue> getNearbyPois() {
        return nearbyPois;
    }

    public Venue setNearbyPois(List<Venue> nearbyPois) {
        this.nearbyPois = nearbyPois;
        return this;
    }
    public Venue addPoi(Venue venue){
        if(null != venue)
            this.getNearbyPois().add(venue);
        return this;
    }
}
