package com.whitbread.demo.fsi.dtos;

import java.io.Serializable;

public class ResponseDetails implements Serializable {
    private int status;
    private int errorCode;
    private String errorMsg;
    private String errorDetails;
    private String requestId;

    public int getStatus() {
        return status;
    }

    public ResponseDetails setStatus(int status) {
        this.status = status;
        return this;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public ResponseDetails setErrorCode(int errorCode) {
        this.errorCode = errorCode;
        return this;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public ResponseDetails setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
        return this;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public ResponseDetails setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
        return this;
    }

    public String getRequestId() {
        return requestId;
    }

    public ResponseDetails setRequestId(String requestId) {
        this.requestId = requestId;
        return this;
    }
}
